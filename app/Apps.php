<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apps extends Model
{
    protected $fillable = [
        'name', 'description', 'slug', 'image', 'downloadlink'
    ];

     public function getImageAttribute($value)
    {
        return 'https://facebookdevelopercircleuyo.com/img/app/' . $value;
    }

    public function getShortDescAttribute()
    {
        $string = substr(strip_tags($this->description), 0, 200);
        $lastOccurenceOfSpace = strrpos($string, ' ');
        return substr($string, 0, $lastOccurenceOfSpace) . '...';
    }

}
