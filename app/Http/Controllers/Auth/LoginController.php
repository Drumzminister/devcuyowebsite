<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\User;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function redirectToFacebookProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderFacebookCallback()
    {
        $user = Socialite::driver('facebook')->stateless()->user(); 

        $existingUser = User::whereEmail($user->email)->first();
        

        if(!$existingUser){
            $existingUser = User::create([
                'name' => $user->name,
                'email' => $user->email,
                'token' => $user->token,
                'avatar' => $user->avatar
            ]);

            Auth::login($existingUser);
            return redirect('/');
        }

        $existingUser->token = $user->token;
        $existingUser->save();

        Auth::login($existingUser);
        return redirect('/');
    }
}
