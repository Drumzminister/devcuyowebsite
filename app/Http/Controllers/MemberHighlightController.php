<?php

namespace App\Http\Controllers;

use Session;
use Storage;
use App\MemberHighlight;
use Illuminate\Http\Request;
use App\Http\Resources\MemberHighlight as MemberHighlightResource;
use App\Http\Resources\MemberHighlightCollection;

class MemberHighlightController extends Controller
{

    public function index($limit = null)
    {
        if($limit === null){
            return new MemberHighlightCollection(MemberHighlight::all());
        }

        return new MemberHighlightCollection(MemberHighlight::limit($limit)->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'image' => 'required|image'
        ]);
       $request['slug'] = str_slug($request->name);

         // Image Upload
        $image = $request->file('image');
        $imageName = str_slug($request->name) . '-' . time() . '.' . $image->getClientOriginalExtension();

        if(! $path = $request->image->storeAs('membershighlight', $imageName)){
            return back()->with('success','Image not Upload successful');
        }

        $member = new MemberHighlight;
        $member->name = $request->name;
        $member->slug = $request->slug;
        $member->description = $request->description;
        $member->image = $imageName;
        $member->save();

        Session::flash('success', 'Member Created!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MemberHighlight  $memberHighlight
     * @return \Illuminate\Http\Response
     */
    public function show(MemberHighlight $memberHighlight)
    {
        return new MemberHighlightResource($memberHighlight);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MemberHighlight  $memberHighlight
     * @return \Illuminate\Http\Response
     */
    public function edit(MemberHighlight $memberHighlight)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MemberHighlight  $memberHighlight
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MemberHighlight $memberHighlight)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MemberHighlight  $memberHighlight
     * @return \Illuminate\Http\Response
     */
    public function destroy(MemberHighlight $memberHighlight)
    {
        $memberHighlight->delete();

        return response()->json(null, 204);
    }

}
