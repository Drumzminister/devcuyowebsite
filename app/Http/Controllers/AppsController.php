<?php

namespace App\Http\Controllers;

use Session;
use App\Apps;
use Storage;
use Illuminate\Http\Request;
use App\Http\Resources\Apps as AppResource;
use App\Http\Resources\AppsCollection;

class AppsController extends Controller
{

    public function index($limit = 6)
    {
        return new AppsCollection(Apps::limit($limit)->get());
    }

    public function allApps()
    {
        return new AppsCollection(Apps::paginate(9));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'image' => 'required|image',
            'downloadlink' => 'required',

        ]);
        $request['slug'] = str_slug($request->name);

         // Image Upload
        $image = $request->file('image');
        $imageName = str_slug($request->name) . '-' . time() . '.' . $image->getClientOriginalExtension();

        if(! $path = $request->image->storeAs('app', $imageName)){
            return back()->with('success','Image not Upload successful');
        }



        // $request->image->storeAs('public/app', $imageName);

        $app = new Apps;
        $app->name = $request->name;
        $app->slug = $request->slug;
        $app->description = $request->description;
        $app->downloadlink = $request->downloadlink;
        $app->image = $imageName;
        $app->save();

        Session::flash('success', 'App Created!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Apps  $apps
     * @return \Illuminate\Http\Response
     */
    public function show(Apps $apps)
    {
        return new AppResource($app);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Apps  $apps
     * @return \Illuminate\Http\Response
     */
    public function edit(Apps $apps)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Apps  $apps
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Apps $apps)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Apps  $apps
     * @return \Illuminate\Http\Response
     */
    public function destroy(Apps $apps)
    {
        $apps->delete();

        return response()->json(null, 204);
    }
}
