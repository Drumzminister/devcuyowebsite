<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class FacebookController extends Controller
{
    public function retrieveMembers(){
        $token = $this->retrieveToken();
        return $token;
    }

    public function retrieveToken(){
        return Auth::user()->token ?? null;
    }
}
