<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Apps extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         return [
            'id'         => $this->id,
            'slug'       => $this->slug,
            'name'       => $this->name,
            'description'    => $this->description,
            'shortdesc'    => $this->shortdesc,
            'downloadlink'    => $this->downloadlink,
            'image'     => $this->image,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
