<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberHighlight extends Model
{
     protected $fillable = [
        'name', 'description', 'slug', 'image'
    ];

    public function getImageAttribute($value)
    {
        return 'https://facebookdevelopercircleuyo.com/img/membershighlight/' . $value;
    }

    public function getShortDescAttribute()
    {
        $string = substr(strip_tags($this->description), 0, 200);
        $lastOccurenceOfSpace = strrpos($string, ' ');
        return substr($string, 0, $lastOccurenceOfSpace) . '...';
    }

}
