<?php

use Illuminate\Database\Seeder;

class MemberHighlightSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\MemberHighlight::class, 10)->create()->each(function($member){
            $member->slug = str_slug($member->name);
            $member->save();
        });
        factory(App\User::class, 10)->create();
    }
}
