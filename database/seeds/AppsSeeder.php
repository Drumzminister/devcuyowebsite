<?php

use Illuminate\Database\Seeder;

class AppsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Apps::class, 10)->create()->each(function($app){
            $app->slug = str_slug($app->name);
            $app->save();
        });

    }
}
