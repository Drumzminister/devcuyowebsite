<?php

use Faker\Generator as Faker;

$factory->define(App\Apps::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'image' => $faker->image('storage/app/public/app', 400, 400, null, false),
        'description' => $faker->sentence,
        'downloadlink' => $faker->text,
    ];
});
