<?php

use Faker\Generator as Faker;

$factory->define(App\MemberHighlight::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'image' => $faker->image('storage/app/public/membershighlight', 400, 400, null, false),
        'description' => $faker->sentence,
    ];
});
