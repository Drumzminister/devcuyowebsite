import React from "react";
import { Link } from "react-router-dom";
import ErrorBoundary from "./ErrorBoundary";

class Showmember extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="col-lg-4 col-md-4 col-sm-10 col-xs-10 col-p-2 mt-4 h-100">
				<div className="card">
					<div className="card-header">
						<img
							className=""
							height="350px"
							width="100%"
							src={this.props.member.image}
							alt={this.props.member.name}
						/>
					</div>
					<div className="card-body shadow h-75">
						<div className="py-2 text-center text-white">
							<h3 className="font-weight-bold">
								{this.props.member.name}
							</h3>
							<p className="text-justify">{this.props.member.shortdesc}</p>
						</div>
						<div className="px-4 py-3 card-link">
							<Link
								to={{
									pathname: "/memberhighlight",
									state: { member: this.props.member }
								}}
							>
								<a className="px-5 btn">
									Read Story{" "}
									<i className="fa fa-long-arrow-right" />
								</a>
							</Link>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Showmember;
