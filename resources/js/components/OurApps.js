import React from "react";
import { Link } from "react-router-dom";
import ErrorBoundary from "./ErrorBoundary";
import ShowApp from "./ShowApp";

class OurApps extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            apps: ""
        };
    }
    componentDidMount() {
        axios
            .get("https://facebookdevelopercircleuyo.com/api/app")
            .then(response => {
                this.setState({
                    apps: response.data.data.map(resp => resp)
                });
            })
            .catch(error => {
                console.log("Error retrieving App");
            });
    }

    render() {
        const apps = Array.from(this.state.apps).map((data, key) => (
            <ShowApp app={data} key={data.id} />
        ));

        return (
            <div className="container-fluid px-0">
                <div className="row page-header app-header">
                    <div className="col-12 col-lg-7">
                        <div className=" text-white text-center py-5 my-5">
                            <div className="py-5 my-5 px-5">
                                <h1 className="font-weight-bold">
                                    Our Community Products
                                </h1>
                                <p>
                                    Here are a list of softwares built by our
                                    community members
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-lg-5 d-none d-lg-block">
                        <div className="app-demo pull-right p-4">
                            <div className="embed-responsive embed-responsive-16by9">
                                <video
                                    className="embed-responsive-item"
                                    controls
                                    src="/vid/fbdevelopers.mp4"
                                    allowFullScreen
                                />
                            </div>
                            <h4>Developers Circle, Uyo</h4>
                            <p>
                                The Developer Circle in Uyo is at the heart of a
                                vibrant and growing Nigerian tech ecosystem.
                            </p>
                        </div>
                    </div>
                </div>
                <div className="container-fluid app-page">
                    {/* App Section Heading */}
                    <div className="text-white py-5 ml-5">
                        <h1 className="font-weight-bold ml-5 px-5">Our Apps</h1>
                    </div>
                    {/* App Section Content */}
                    <div className="justify-content-center py-4 px-5 container">
                        <div className="row justify-content-center">
                            <ErrorBoundary>{apps}</ErrorBoundary>
                        </div>
                    </div>
                    <div className="d-none d-lg-block">
                        <br />
                        <br />
                        <br />
                    </div>
                </div>
                {/* End App Section Content */}
            </div>
        );
    }
}

export default OurApps;
