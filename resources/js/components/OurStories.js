import React from "react";
import { Link } from "react-router-dom";
import MemberHighlight from "./MemberHighlight";
import ErrorBoundary from "./ErrorBoundary";
import ShowMember from "./ShowMember";

class OurStories extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            members: ""
        };
        this.getMember = this.getMember.bind(this);
    }
    getMember() {
        axios
            .get("https://facebookdevelopercircleuyo.com/api/highlight")
            .then(response => {
                this.setState({
                    members: response.data.data.map(resp => resp)
                });
            })
            .catch(error => {
                console.log("Error retrieving Member");
            });
    }
    componentDidMount() {
        this.getMember();
    }
    render() {
        //Retrieve member details from state and render in child component with passed props
        const members = Array.from(this.state.members).map((data, key) => (
            <ShowMember member={data} key={data.id} />
        ));

        return (
            <div className="container-fluid px-0">
                <div className="row page-header stories-header">
                    <div className="col-12 col-lg-7">
                        <div className=" text-white text-center py-5">
                            <div className="py-5 my-5 px-5">
                                <h1 className="font-weight-bold">
                                    Our Community Stories
                                </h1>
                                <p>
                                    Here are a list of stories by our community
                                    members
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-lg-5 d-none d-lg-block">
                        <div className="app-demo pull-right p-4">
                            <div className="embed-responsive embed-responsive-16by9">
                                <video
                                    className="embed-responsive-item"
                                    controls
                                    src="/vid/fbdevelopers.mp4"
                                    allowFullScreen
                                />
                            </div>
                            <h4>Developers Circle, Uyo</h4>
                            <p>
                                The Developer Circle in Uyo is at the heart of a
                                vibrant and growing Nigerian tech ecosystem.
                            </p>
                        </div>
                    </div>
                </div>
                <div className="container-fluid app-page">
                    {/* Stories Section Heading */}
                    <div className="text-white py-5 ml-5">
                        <h1 className="font-weight-bold ml-5 px-5">
                            Our Stories
                        </h1>
                    </div>
                    {/* Stories Section Content */}
                    <section className="membership-stories py-5">
                        <div className="container">
                            <div className="row justify-content-center">
                                <ErrorBoundary>{members}</ErrorBoundary>
                            </div>
                        </div>
                    </section>

                    <div className="d-none d-lg-block">
                        <br />
                        <br />
                        <br />
                    </div>
                </div>
                {/* End Stories Section Content */}
            </div>
        );
    }
}

export default OurStories;
