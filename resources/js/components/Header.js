import React from "react";
import { Link } from "react-router-dom";

const Header = () => (
  <div className="d-flex flex-column flex-md-row align-items-center px-3 my-0 bg-dark text-light fixed-top">
    <Link className="my-0 mr-md-auto font-weight-bold text-light" to="/">
      <img
        className="img-fluid h-100"
        src="/img/devcuyo-logo.png"
        alt="DevC Uyo logo"
      />
    </Link>
    <nav className="navbar navbar-dark my-md-0">
      <Link className="p-2 text-light" to="/membership">
        Membership
      </Link>
      <a href="https://medium.com/@facebookdevcuyo" className="p-2 text-light">
        Blog
      </a>
      <a
        className="p-2 dropdown-toggle text-light"
        href="#"
        id="navbarDropdownMenuLink"
        role="button"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        About us
      </a>
      <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        <Link className="p-2 text-dark dropdown-item" to="/ourapps">
          Our Apps
        </Link>
        <Link className="p-2 text-dark dropdown-item" to="/stories">
          Our Stories
        </Link>
        {/* <Link className="p-2 text-dark dropdown-item" to='/members'>Members</Link> */}
        <a href="/members" className="p-2 text-dark dropdown-item">
          Members
        </a>
      </div>
    </nav>
  </div>
);
export default Header;
