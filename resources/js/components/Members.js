import axios from "axios";
import React, { Component } from "react";
import { Link } from "react-router-dom";

const Members = () => (
    <div className="container-fluid bg-white my-3 p-0">
        <div className="container-fluid py-3">
            <h1 className="font-weight-bold">Our Members</h1>
        </div>
        {/* Admin Member Section 2 */}
        <div className="row container-fluid py-4 my-7 justify-content-center text-center">
            <h1 className="font-weight-bold">Admin</h1>
            <div className="row container-fluid justify-content-center text-center">
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
            </div>
        </div>
        {/* End Admin Section 2 */}

        {/* Moderator Member Section 2 */}
        <div className="row container-fluid py-4 my-7 justify-content-center text-center">
            <h1 className="font-weight-bold">Moderators</h1>
            <div className="row container-fluid justify-content-center text-center">
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
            </div>
        </div>
        {/* End Moderator Section 2 */}

        {/* All Member Section 2 */}
        <div className="row container-fluid py-4 my-7 justify-content-center text-center">
            <h1 className="font-weight-bold">All members</h1>
            <div className="row container-fluid justify-content-center text-center">
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
                <div className="col-xs-4 px-5 my-2 mx-4 justify-content-center text-center">
                    <img
                        className="member-img p-0 m-0"
                        src="/img/hanson.jpeg"
                        alt="Dev C"
                    />
                    <p className="font-weight-bold my-4">Hanson Johnson</p>
                </div>
            </div>
        </div>
        {/* End All Member Section 2 */}
    </div>
);
export default Members;
