import axios from "axios";
import React, { Component } from "react";
import { Link } from "react-router-dom";

class Contact extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="container-fluid align-items-center mt-5">
                <form>
                    <div className="form-group col-sm-10">
                        <label for="InputEmail1">Email address</label>
                        <input
                            type="email"
                            className="form-control"
                            id="InputEmail1"
                            aria-describedby="emailHelp"
                            placeholder="Enter email"
                        />
                        <small id="emailHelp" className="form-text text-muted">
                            We'll never share your email with anyone else.
                        </small>
                    </div>
                    <div className="form-group col-sm-10">
                        <label for="InputName1">Password</label>
                        <input
                            type="name"
                            className="form-control"
                            id="InputName1"
                            placeholder="Password"
                        />
                    </div>
                    <div className="form-group col-sm-10">
                        <label for="FormControlTextarea1">
                            Example textarea
                        </label>
                        <textarea
                            className="form-control"
                            id="FormControlTextarea1"
                            rows="3"
                        />
                    </div>
                    <div className="form-group form-check">
                        <input
                            type="checkbox"
                            className="form-check-input"
                            id="exampleCheck1"
                        />
                        <label className="form-check-label" for="exampleCheck1">
                            Check me out
                        </label>
                    </div>
                    <button type="submit" className="btn btn-primary">
                        Submit
                    </button>
                </form>
            </div>
        );
    }
}

export default Contact;
