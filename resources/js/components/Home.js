import axios from "axios";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import MemberHighlight from "./MemberHighlight";
import OurApps from "./OurApps";
import ErrorBoundary from "./ErrorBoundary";
import ShowApp from "./ShowApp";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      memberOne: "",
      memberTwo: "",
      apps: ""
    };
    this.getMember = this.getMember.bind(this);
    this.getApp = this.getApp.bind(this);
  }

  getMember() {
    axios
      .get("https://facebookdevelopercircleuyo.com/api/highlight")
      .then(response => {
        this.setState({
          memberOne: response.data.data[5],
          memberTwo: response.data.data[1]
        });
      })
      .catch(error => {
        console.log("Error retrieving Member");
      });
  }

  getApp() {
    axios
      .get("https://facebookdevelopercircleuyo.com/api/app/3")
      .then(response => {
        this.setState({
          apps: response.data.data.map(resp => resp)
        });
      })
      .catch(error => {
        console.log("Error retrieving App");
      });
  }
  componentDidMount() {
    this.getMember();
    this.getApp();
  }
  render() {
    const apps = Array.from(this.state.apps).map((data, key) => (
      <ShowApp app={data} key={data.id} />
    ));

    return (
      <div className="container-fluid m-0 p-0">
        {/* Begin Carousel */}
        <div className=" bg-dark text-light carousel-font">
          <div
            id="carouselExampleIndicators"
            className="carousel slide carousel-indicator"
            data-ride="carousel"
          >
            <ol className="carousel-indicators mr-1">
              <li
                data-target="#carouselExampleIndicators"
                data-slide-to="0"
                className="active"
              />
              <li data-target="#carouselExampleIndicators" data-slide-to="1" />
              <li data-target="#carouselExampleIndicators" data-slide-to="2" />
              <li data-target="#carouselExampleIndicators" data-slide-to="3" />
            </ol>
            <div className="carousel-inner m-0 p-0">
              <div className="carousel-item active">
                <div className="row justify-content-center align-items-center">
                  <div className="col-sm-6">
                    <img
                      src="/img/f8.png"
                      alt="Dev C"
                      height="518px"
                      width="100%"
                    />
                  </div>
                  <div className="col-sm-6 justify-content-center">
                    <h5>F8 2019 Meetup: Uyo</h5>
                    <h1 className="mb-5 font-weight-bold">
                      F8, Facebook’s annual developer conference, is happening
                      on April 30 and May 1 in San Jose, CA.{" "}
                    </h1>
                    <a href="https://devcuyof8watchparty2019.splashthat.com/">
                      <button className="btn btn-dark border btn-lg  px-5">
                        Read More
                      </button>
                    </a>
                  </div>
                </div>
              </div>
              <div className="carousel-item">
                <div className="row justify-content-center align-items-center">
                  <div className="col-sm-6">
                    <img
                      src="/img/group-photo.jpeg"
                      alt="Dev C"
                      height="518px"
                      width="100%"
                    />
                  </div>
                  <div className="col-sm-6 justify-content-center">
                    <h5>Social Presence</h5>
                    <h1 className="mb-5 font-weight-bold">
                      Building Social experience <br /> to bring people together
                    </h1>
                    <a href="https://developers.facebook.com/products#social-presence">
                      <button className="btn btn-dark border btn-lg  px-5">
                        Read More
                      </button>
                    </a>
                  </div>
                </div>
              </div>
              <div className="carousel-item">
                <div className="row h-100 justify-content-center align-items-center">
                  <div className="col-sm-6">
                    <img
                      src="/img/cross-section.jpeg"
                      alt="Dev C"
                      height="518px"
                      width="100%"
                    />
                  </div>
                  <div className="col-sm-6 justify-content-center">
                    <h5>Hackathon</h5>
                    <h1 className="mb-5 font-weight-bold">Hack! Hack! Hack!</h1>
                    <a href="https://developers.facebook.com/products#open-source">
                      <button className="btn btn-dark border btn-lg  px-5">
                        Read More
                      </button>
                    </a>
                  </div>
                </div>
              </div>
              <div className="carousel-item">
                <div className="row h-100 justify-content-center align-items-center">
                  <div className="col-sm-6">
                    <img
                      src="/img/learning.jpeg"
                      alt="Dev C"
                      height="518px"
                      width="100%"
                    />
                  </div>
                  <div className="col-sm-6 justify-content-center">
                    <h5>Meetups</h5>
                    <h1 className="mb-5 font-weight-bold">
                      Encouraging growth and Development
                    </h1>
                    <a href="https://developers.facebook.com/products#social-integrations">
                      <button className="btn btn-dark border btn-lg px-5">
                        Read More
                      </button>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* End Carousel; Begin Explore */}
        <div className="container-fluid pt-5 explore-section">
          {/* Explore Section Heading */}
          <div className="container-fluid py-lg-3">
            <h1 className="font-weight-bold text-center">Explore</h1>
          </div>
          <div className="py-3 p-md-0" />
          {/* Explore Section 1 */}
          <div className="row container-fluid py-lg-4 my-lg-4">
            {/* Explore Content Section 1 */}
            <div className="row container-fluid px-md-5">
              {/* Explore Section 1 Images */}
              <div className="col-6 row px-5 d-none d-lg-block">
                <img
                  className="img-fluid img-one col-sm-6 p-0 m-0"
                  src="/img/connecting people 1.JPG"
                  alt="Dev C"
                />
                <img
                  className="img-fluid img-two col-sm-6 p-0 m-0 align-self-center"
                  src="/img/connecting people 2.JPG"
                  alt="Dev C"
                />
              </div>
              {/* Explore Section 1 Text  */}
              <div className="col-12 col-lg-6 py-md-5 justify-content-center">
                <h2 className="font-weight-bold pb-2 d-none d-lg-block">
                  Connecting people with <br /> facebook.
                </h2>
                <h2 className="font-weight-bold pb-2 d-lg-none">
                  Connecting people with facebook.
                </h2>
                <p>
                  We are building a global community where reaching everyone
                  from everywhere is possible. We are building a community where
                  ideas and knowledge can be shared without any barriers of
                  distance, language or expertise. Together, we are ensuring
                  that communication becomes easier in the nearest future.
                </p>
              </div>
            </div>
            {/* End Explore Section 1 Content */}
          </div>
          {/* End Explore Section 1; Begin Explore Section 2*/}
          <div className="py-3 p-md-0" />

          <div className="row container-fluid py-md-5 my-lg-5">
            {/* Explore Content Section 2 */}
            <div className="row container-fluid px-md-5">
              {/* Explore Section 2 Text  */}
              <div className="col-12 col-lg-6 p-lg-5 justify-content-center">
                <h2 className="font-weight-bold pb-2 d-none d-lg-block">
                  Bringing your imaginations to <br /> life.
                </h2>
                <h2 className="font-weight-bold pb-2 d-lg-none">
                  Bringing your imaginations to life.
                </h2>
                <p>
                  We are a body of determined spirit powered by the unquenchable
                  love for innovative solutions. We are always ready to work
                  with you, to assist you in the journey of bringing your
                  projects to life, by providing you the best tools, teams and
                  partnerships you need to excel in your innovative journey.
                </p>
              </div>
              {/* Explore Section 2 Images */}
              <div className="col-sm-6 row px-5 d-none d-lg-block">
                <img
                  className="img-fluid img-two col-sm-6 p-0 m-0 align-self-center"
                  src="/img/bringing to life 1.jpg"
                  alt="Dev C"
                />
                <img
                  className="img-fluid img-one col-sm-6 p-0 m-0"
                  src="/img/bringtolive.jpg"
                  alt="Dev C"
                />
              </div>
            </div>
            {/* End Explore Section 2 Content*/}
          </div>
          {/* Explore Section 3 */}
          <div className="row container-fluid py-md-5 pt-lg-5 mt-lg-5">
            {/* Explore Content Section 3 */}
            <div className="row container-fluid px-md-5">
              {/* Explore Section 3 Images */}
              <div className="col-sm-6 row px-5 d-none d-lg-block">
                <img
                  className="img-fluid img-one col-sm-6 p-0 m-0"
                  src="/img/contribute codes 1.jpg"
                  alt="Dev C"
                />
                <img
                  className="img-fluid img-two col-sm-6 p-0 m-0 align-self-center"
                  src="/img/contribute codes 2.jpg"
                  alt="Dev C"
                />
              </div>
              {/* Explore Section 3 Text  */}
              <div className="col-12 col-lg-6 p-lg-5 justify-content-center">
                <h2 className="font-weight-bold pb-2 d-none d-lg-block">
                  Contribute codes to accelerate <br /> projects.
                </h2>
                <h2 className="font-weight-bold pb-2 d-lg-none">
                  Contribute codes to accelerate projects.
                </h2>
                <p>
                  Together, our team provides first hand assistance in
                  contributing codes to projects. We believe in being on time,
                  being better than we were and growing our skill base together.
                  We are always ready to assist with debugging and rearranging
                  your codes, using the best technologies available to enhance
                  product optimization.
                </p>
              </div>
            </div>
            {/* End Explore Section 3 Content */}
          </div>
          {/* End Explore Section */}
        </div>
        {/* End Explore Section; Begin Member Highlight Section */}
        <div className="container-fluid bg-white my-3 members">
          {/* Member Highlight Section Heading */}
          <div className="py-3">
            <h1 className="font-weight-bold text-center">
              Member's Highlights
            </h1>
          </div>
          {/* Member Highlight Section 1 */}
          <div className="row container-fluid py-lg-4 my-md-4 mx-auto">
            {/* Member Highlight Content Section 1 */}
              {/* Member Highlight Section 1 Images */}
              <div className="col-12 col-md-6 px-lg-5 text-md-center">
                <img
                  className="p-0 m-0 img-fluid memberhighlight-img"
                  src={this.state.memberOne.image}
                  alt="Dev C"
                />
              </div>
              {/* Member Highlight Section 1 Description  */}
              <div className="col-12 col-md-6 offset-md-3 offset-lg-0 p-lg-5 mt-3 py-4 mt-md-0 justify-content-center align-self-center">
                <h3 className="font-weight-bold">
                  {this.state.memberOne.name}
                </h3>
                <p className="lead text-justify">
                  {this.state.memberOne.shortdesc}
                </p>
                <Link
                  to={{
                    pathname: "/memberhighlight",
                    state: { member: this.state.memberOne }
                  }}
                >
                  <button className="btn btn-white border-dark">
                    Read More
                  </button>
                </Link>
              </div>
            {/* End Member Highlight Section 1 Content */}
          </div>
          {/* End Member Highlight Section 1; Begin Member Highlight Section 2*/}
          {/* Member Highlight Section 2 */}
          <ErrorBoundary>
            <div className="row container-fluid py-lg-4 my-md-4 mx-auto">
              {/* Member Highlight Content Section 2 */}
                {/* Member Highlight Section 2 Description  */}
                <div className="col-12 col-md-6 p-lg-5 py-4 justify-content-center align-self-center">
                  <h3 className="font-weight-bold">
                    {this.state.memberTwo.name}
                  </h3>
                  <p className="lead text-justify">
                    {this.state.memberTwo.shortdesc}
                  </p>
                  <Link
                    to={{
                      pathname: "/memberhighlight",
                      state: { member: this.state.memberTwo }
                    }}
                  >
                    <button className="btn btn-white border-dark">
                      Read More
                    </button>
                  </Link>
                </div>
                {/* Member Highlight Section 2 Images */}
                <div className="col-12 col-md-6 px-lg-5 order-first order-md-last">
                  <img
                    className="p-0 m-0 img-fluid memberhighlight-img"
                    src={this.state.memberTwo.image}
                    alt="member pic"
                  />
                </div>
              {/* End Member Highlight Section 2 Content */}
            </div>
          </ErrorBoundary>
        </div>
        {/* End Member Highlight Section; Begin Our Apps Section */}
        <section>
          <div className="container app-section py-3">
            <div className="row pb-4">
              <div className="col-12 text-center">
                <h1 className="font-weight-bold text-center">Our Apps</h1>
              </div>
            </div>
            <div className="row justify-content-center">
              <ErrorBoundary>{apps}</ErrorBoundary>
            </div>
            <div className="row pt-3 pb-4">
              <div className="col-12 col-md-6 mx-auto text-center">
                <Link
                  className="btn btn-lg px-md-5 btn-outline-dark text-black"
                  to="/ourapps"
                >
                  See More Apps
                </Link>
              </div>
            </div>
          </div>
        </section>

        <div className="row justify-content-center p-5">
          <div className="embed-responsive embed-responsive-16by9 p-7 w-75">
            <video
              className="embed-responsive-item"
              src="/vid/fbdevelopers.mp4"
              allowFullScreen
              controls
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
