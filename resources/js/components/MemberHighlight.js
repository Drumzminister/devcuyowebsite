import React from "react";
import { Link } from "react-router-dom";
import renderHTML from 'react-render-html';
import ErrorBoundary from "./ErrorBoundary";

class MemberHighlight extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="row container-fluid bg-white p-0">
                <div className="col-xs-12 container-fluid member-highlight-wrapper text-justify py-3 m-4 p-5">
                    <div className="pull-left">
                        <img
                            className="member-highlight-img px-4"
                            src={this.props.location.state.member.image}
                            alt="Dev C"
                        />
                    </div>
                    <div className="">
                        <h2 className="font-weight-bold">
                            {" "}
                            {this.props.location.state.member.name}{" "}
                        </h2>{" "}
                        <br />
                        {renderHTML(this.props.location.state.member.description)}{" "}
                    </div>
                </div>
            </div>
        );
    }
}

export default MemberHighlight;
