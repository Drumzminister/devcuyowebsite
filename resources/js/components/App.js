import React, { Component } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Header from "./Header";
import Home from "./Home";
import Members from "./Members";
import Membership from "./Membership";
import About from "./About";
import MemberHighlight from "./MemberHighlight";
import OurStories from "./OurStories";
import Footer from "./Footer";
import Contact from "./Contact";
import OurApps from "./OurApps";
import ScrollToTop from "./ScrollToTop";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            members: "",
            apps: ""
        };
    }
    componentDidMount() {
        axios
            .get("https://facebookdevelopercircleuyo.com/api/highlight")
            .then(response => {
                this.setState({
                    members: [...response.data.data],
                    apps: [...response.data.data]
                });
                return axios.get(
                    "https://facebookdevelopercircleuyo.com/api/app/6"
                );
            })
            .catch(error => {
                console.log(error);
            });
    }
    render() {
        return (
            <BrowserRouter>
                <ScrollToTop>
                    <Header />
                    <Switch>
                        <Route
                            path="/"
                            exact
                            render={routeProps => (
                                <Home
                                    {...routeProps}
                                    members={this.state.members}
                                    apps={this.state.apps}
                                />
                            )}
                        />
                        <Route path="/membership" component={Membership} />
                        {/* <Route path='/members' component={Members} /> Not ready*/}
                        <Route path="/stories" component={OurStories} />
                        <Route path="/contact" component={Contact} />
                        <Route path="/about" component={About} />
                        <Route
                            path="/memberhighlight"
                            component={MemberHighlight}
                        />
                        <Route
                            path="/ourapps"
                            render={routeProps => (
                                <OurApps
                                    {...routeProps}
                                    apps={this.state.apps}
                                />
                            )}
                        />
                    </Switch>
                    <Footer />
                </ScrollToTop>
            </BrowserRouter>
        );
    }
}

ReactDOM.render(<App />, document.getElementById("app"));
