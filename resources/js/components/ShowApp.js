import React from "react";
import { Link } from "react-router-dom";
import ErrorBoundary from "./ErrorBoundary";

export default class ShowApp extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div className="col-12 col-md-4 text-center py-3">
				<div className="bg-primary py-2 px-2 text-white">
					<div className="card-header">
						<img
							className="img-fluid"
							src={this.props.app.image}
							alt="syde"
						/>
					</div>
					<div className="card-body">
						{this.props.app.description}
					</div>
					<div className="px-2 py-3 card-link">
						<a href={this.props.app.downloadlink} className="px-5 btn">
							Download <i className="fa fa-long-arrow-right" />
						</a>
					</div>
				</div>
			</div>
		);
	}
}
