import axios from "axios";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import MemberHighlight from "./MemberHighlight";
import ErrorBoundary from "./ErrorBoundary";
import ShowMember from "./ShowMember";

class Membership extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hover: [true, true, true, true],
            members: ""
        };
        this.getMember = this.getMember.bind(this);
    }

    toggleHover(element) {
        let temp = this.state.hover;
        temp[element] = !temp[element];
        this.setState({
            hover: temp
        });
    }

    getMember() {
        axios
            .get("https://facebookdevelopercircleuyo.com/api/highlight/3")
            .then(response => {
                this.setState({
                    members: response.data.data.map(resp => resp),
                    memberOne: response.data.data[0],
                    memberTwo: response.data.data[0]
                });
            })
            .catch(error => {
                console.log("Error retrieving Member");
            });
    }
    componentDidMount() {
        this.getMember();
    }
    render() {
        //Retrieve member details from state and render in child component with passed props
        const members = Array.from(this.state.members).map((data, key) => (
            <ShowMember member={data} key={data.id} />
        ));

        return (
            <div>
                <div className="page-header container-fluid text-center text-white stories-header membership-header1">
                    <div className="mt-5 py-5">
                        <img className="img-fluid h-25" src="/img/logo.png" />
                        <h1 className="font-weight-bold pt-3">
                            Connecting communities to develop the future.
                        </h1>
                        <p>
                            Attend social meetups, collaborate in Facebook
                            groups and learn about the latest Facebook
                            technologies.
                        </p>
                        <a
                            href="https://web.facebook.com/groups/DevCUyo"
                            className="btn btn-outline-info text-white border-white"
                        >
                            Join our circle
                        </a>
                    </div>
                </div>
                <div
                    className="row p-5 m-0 justify-content-center text-center membership-header2"
                    style={{ backgroundColor: "#C4C4C4" }}
                >
                    <div className="col-12">
                        <div className="flex-sm-column my-4">
                            <h1 className="membership-heading mb-3">
                                Why Join our circle?
                            </h1>
                            <h5 className="text-center mb-3">
                                Whatever you build, Developer Circles connects
                                you to collaborate, <br />
                                learn, and code with other local developers.
                            </h5>
                        </div>
                    </div>
                    <div className="col-12">
                        <div className="row my-5">
                            <div
                                className="col-12 col-md-2 mx-md-5 my-4 circle-div"
                                onMouseEnter={() => this.toggleHover(0)}
                                onMouseLeave={() => this.toggleHover(0)}
                            >
                                {this.state.hover[0] ? (
                                    [
                                        <img
                                            className="circle-img"
                                            src="/img/other-developers.png"
                                            alt="Dev C"
                                        />,
                                        <p className="circle-text">
                                            Connect with other developers
                                        </p>
                                    ]
                                ) : (
                                    <p className="circle-text">
                                        Access an exclusive local Facebook Group
                                        community and attend our local meetups
                                    </p>
                                )}
                            </div>
                            <div
                                className="col-12 col-md-2 mx-md-5 my-4 circle-div"
                                onMouseEnter={() => this.toggleHover(1)}
                                onMouseLeave={() => this.toggleHover(1)}
                            >
                                {this.state.hover[1] ? (
                                    [
                                        <img
                                            className="circle-img"
                                            src="/img/local-experts.png"
                                            alt="Dev C"
                                        />,
                                        <p className="circle-text">
                                            Engage with local experts
                                        </p>
                                    ]
                                ) : (
                                    <p className="circle-text">
                                        Our community is organized and run by
                                        local developer Leads
                                    </p>
                                )}
                            </div>
                            <div
                                className="col-12 col-md-2 mx-md-5 my-4 circle-div"
                                onMouseEnter={() => this.toggleHover(2)}
                                onMouseLeave={() => this.toggleHover(2)}
                            >
                                {this.state.hover[2] ? (
                                    [
                                        <img
                                            className="circle-img"
                                            src="/img/about-new-tech.png"
                                            alt="Dev C"
                                        />,
                                        <p className="circle-text">
                                            Learn about new tech
                                        </p>
                                    ]
                                ) : (
                                    <p className="circle-text">
                                        Learn about Bots, AI, IoT, React and
                                        other tools
                                    </p>
                                )}
                            </div>
                            <div
                                className="col-12 col-md-2 mx-md-5 my-4 circle-div"
                                onMouseEnter={() => this.toggleHover(3)}
                                onMouseLeave={() => this.toggleHover(3)}
                            >
                                {this.state.hover[3] ? (
                                    [
                                        <img
                                            className="circle-img"
                                            src="/img/build-with-others.png"
                                            alt="Dev C"
                                        />,
                                        <p className="circle-text">
                                            Build with other developers
                                        </p>
                                    ]
                                ) : (
                                    <p className="circle-text">
                                        Collaborate with developers of all types
                                    </p>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="page-header join-bg container-fluid text-white py-5 membership-header2">
                    <div className="row">
                        <div className="col-12 my-5">
                            <h1 className="font-weight-bold">
                                Join our community
                            </h1>
                            <p>
                                Join our local Developer Circle to connect with
                                your developer community on <br /> Facebook and
                                attend meetups in your area.
                            </p>
                            <a target="_blank" href="https://www.facebook.com/groups/DevCUyo" className="btn text-white border-white btn-outline-dark">
                                Join our circle
                            </a>
                        </div>
                    </div>
                </div>

                <section className="membership-stories py-5">
                    <div className="container">
                        <div className="row justify-content-center">
                            <ErrorBoundary>{members}</ErrorBoundary>
                        </div>
                        <div className="row pb-5 mt-5">
                            <div className="col-12 col-md-6 mx-auto text-center">
                                <Link
                                    className="btn btn-lg px-md-5 btn-outline-light"
                                    to="/stories"
                                >
                                    See More Stories
                                </Link>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Membership;
