import React from "react";
import { Link } from "react-router-dom";

const Footer = () => (
  <footer className="bg-dark text-light container-fluid">
    <div className="row">
      <div className="col-4 col-md mt-4 ml-2">
        <h5>Programs</h5>
        <ul className="list-unstyled text-small">
          <li>
            <a
              className="text-muted"
              target="_blank"
              href="https://devcuyoaifiesta.splashthat.com/"
            >
              FB Watch Party
            </a>
          </li>
          <li>
            <a
              className="text-muted"
              target="_blank"
              href="https://hackdayuyo.splashthat.com/"
            >
              Hack Days
            </a>
          </li>
          <li>
            <a
              className="text-muted"
              target="_blank"
              href="https://devcuyoiwd.splashthat.com/"
            >
              International Women's Day
            </a>
          </li>
          <li>
            <a
              className="text-muted"
              target="_blank"
              href="https://devcuyoaifiesta.splashthat.com/"
            >
              AI/AR Workshops
            </a>
          </li>
        </ul>
      </div>
      <div className="col-4 col-md mt-4 ml-2">
        <h5>What we offer</h5>
        <ul className="list-unstyled text-small">
          <li>
            <a
              className="text-muted"
              target="_blank"
              href="https://developers.facebook.com/products#artificial-intelligence"
            >
              Artificial Intelligence
            </a>
          </li>
          <li>
            <a
              className="text-muted"
              target="_blank"
              href="https://developers.facebook.com/products#augmented-reality"
            >
              Augmented Reality
            </a>
          </li>
          <li>
            <a
              className="text-muted"
              target="_blank"
              href="https://developers.facebook.com/products#business-tools"
            >
              Business Tools
            </a>
          </li>
          <li>
            <a
              className="text-muted"
              target="_blank"
              href="https://developers.facebook.com/products#gaming"
            >
              Gaming
            </a>
          </li>
          <li>
            <a
              className="text-muted"
              target="_blank"
              href="https://developers.facebook.com/products#open-source"
            >
              Open Source
            </a>
          </li>
          <li>
            <a
              className="text-muted"
              target="_blank"
              href="https://developers.facebook.com/products#publishing"
            >
              Publishing
            </a>
          </li>
          <li>
            <a
              className="text-muted"
              target="_blank"
              href="https://developers.facebook.com/products#social-integrations"
            >
              Social Integrations
            </a>
          </li>
          <li>
            <a
              className="text-muted"
              target="_blank"
              href="https://developers.facebook.com/products#social-presence"
            >
              Social Presence
            </a>
          </li>
          <li>
            <a
              className="text-muted"
              target="_blank"
              href="https://developers.facebook.com/products#virtual-reality"
            >
              Virtual Reality
            </a>
          </li>
        </ul>
      </div>
      <div className="col-4 col-md mt-4 ml-2">
        <h5>News</h5>
        <ul className="list-unstyled text-small">
          <li>
            <a
              className="text-muted"
              target="_blank"
              href="https://medium.com/@facebookdevcuyo"
            >
              Blog
            </a>
          </li>
          <li>
            <Link className="text-muted" to="/stories">
              Success Stories
            </Link>
          </li>
          <li>
            <a className="text-muted" target="_blank" href="https://www.facebook.com/groups/DevCUyo">
              Developer Circle Uyo Page
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div className="row">
      <div className="col-auto">
        <h5>Follow us</h5>
        <ul className="list-unstyled list-inline social text-center pr-2">
          <li className="list-inline-item pr-2">
            <a target="_blank" href="https://www.facebook.com/groups/DevCUyo">
              <i className="fa fa-facebook-official" />
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div className="row ml-1">
      <p className="text-muted">
        &copy; 2019 DevCUyo &middot;{" "}
                <a target="_blank" href="https://web.facebook.com/FacebookforDevelopers?_rdc=1&_rdr" className="text-muted">
          About
        </a>{" "}
        &middot;{" "}
                <a target="_blank" href="https://web.facebook.com/about/privacy?_rdc=1&_rdr" className="text-muted">
          Privacy
        </a>{" "}
        &middot;{" "}
                <a target="_blank" href="https://web.facebook.com/policies?_rdc=1&_rdr" className="text-muted">
          Terms
        </a>
      </p>
    </div>
  </footer>
);

export default Footer;
