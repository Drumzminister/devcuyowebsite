<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>DevCUyo Member Highlight</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/richtext.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>
    @if(Session::has('success'))
    <div class="alert alert-success">
        <strong>Success!</strong> {{Session::get('success')}}
    </div>
    @endif
    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <div class="jumbotron text-center">
                    <h1>Share a story about an outstanding member of the community.</h1>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-6 mx-auto">
                <form method="POST" action="{{route('highlight.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name" required>
                        @if($errors->has('name'))
                        <span class="invalid">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span> @endif
                    </div>

                   <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <textarea id="description" type="text" class="form-control content" name="description" placeholder="Description" required>{{ old('description') }}</textarea>    @if($errors->has('description'))
                    <span class="invalid">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span> @endif
                </div>

                    <div class="form-row">
                        <div class="form-group">
                            <input type="file" name="image" id="inputImage" placeholder="Upload Image">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mt-5">Add Highlight</button>
                </form>

            </div>
        </div>
    </div>

</body>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="{{asset('js/jquery.richtext.min.js')}}"></script>

<script>
    $(document).ready(function() {
            $('.content').richText();
        });
</script>

</html>
