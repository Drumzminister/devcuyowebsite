<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>DevCUyo</title>
    <!-- Styles -->
    <style>
        .flex-center {
            display: flex;
            flex-direction: column;
            align-items: center;
            height: 100vh;
        }

        .flex-center h1 {
            margin-top: 25vh;
            font-size: 3em;
            color: #2870B3;
        }

        button {
            border-radius: 45px;
            border: none;
            color: white;
            font-size: 1.5em;
            height: 3em;
            background-color: #0AA2E3;
            width: 282px;
            height: 53px;
            margin-top: 10vh;
        }
    </style>
</head>

<body>
    <div class="flex-center">
        <h1>For Admin Login only.</h1>
        <a href="{{route('facebook.login')}}"><button class="btn btn-primary">Login with Facebook</button></a>
    </div>

</body>

</html>
