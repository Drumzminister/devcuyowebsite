<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('facebook/login', 'Auth\LoginController@redirectToFacebookProvider')->name('facebook.login');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderFacebookCallback')->name('callback');
Route::get('getMembersList', 'FacebookController@retrieveMembers');

Route::post('apps', 'AppsController@store')->name('app.store');
Route::post('highlight', 'MemberHighlightController@store')->name('highlight.store');


Route::get('/login', function() {
    return view('auth');
});

Route::get('game', function(){
    return  public_path('/img/app');
});

Route::get('/addhighlight', function() {
    return view('addhighlight');
})->middleware('auth');

Route::get('/addapp', function() {
    return view('addapp');
})->middleware('auth');

Route::get('/members', function () {
    return view('comingsoon');
});

Route::get('/', function () {
    return view('app');
})->name('home');

Route::get('/membership', function () {
    return view('app');
});
Route::get('/ourapps', function () {
    return view('app');
});

Route::get('/stories', function () {
    return view('app');
});

Route::get('/memberhighlight', function () {
    return view('app');
});
